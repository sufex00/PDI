#include <iostream>

#include <opencv2/opencv.hpp>


using namespace std;

using namespace cv;

cv::Mat createWhiteDisk (const int &rows, const int &cols, const int cx, const int cy, const int &radius)
{
    Mat img = Mat::zeros(rows, cols,CV_32F);
    for(int x =0; x <img.cols;x++)
    {
        for(int y=0;y<img.rows;y++)
        {
            float d= sqrt((x-cx)*(x-cx)+(y-cy)*(y-cy));
            if (d<=radius && d >= radius-3)
            {
                img.at<float>(y,x )= 1;
            }

        }
    }
    img = 1 - img;
    return img;
}

cv::Mat ffshift(const Mat &src){
    Mat tmp = src.clone();
    Mat tmp2;

    tmp = tmp (Rect(0,0,tmp.cols & -2, tmp.rows & -2));

    int cx = tmp.cols/2;
    int cy = tmp.rows/2;

    Mat q0(tmp, Rect(0,0,cx,cy));
    Mat q1(tmp, Rect(cx,0,cx,cy));
    Mat q2(tmp, Rect(0,cy,cx,cy));
    Mat q3(tmp, Rect(cx,cy,cx,cy));

    q1.copyTo(tmp2);
    q2.copyTo(q1);
    tmp2.copyTo(q2);

    q0.copyTo(tmp2);
    q3.copyTo(q0);
    tmp2.copyTo(q3);

    return tmp;
}

cv:: Mat scaleImage2_uchar (Mat &src)
{
    Mat tmp = src.clone();
    if (src.type()!= CV_32F){
        tmp.convertTo(tmp, CV_32F);
    }
    normalize(tmp,tmp,1,0, NORM_MINMAX);
    tmp = 255*tmp;
    tmp.convertTo(tmp, CV_8U, 1,0);
    return tmp;
}

cv::Mat appplyLogTransform(const Mat &img){
    Mat  mag = img.clone();
    mag +=1;
    log(mag,mag);
    return mag;
}


int main(int argc, char *argv[])
{
    Mat img = imread("/home/pedro/git/PDI/img/lena_noisy.png", IMREAD_GRAYSCALE);

    Mat disk0= Mat::zeros(img.cols,img.rows, CV_32F);
    Mat disk = disk0.clone();

    Mat planes[] = {Mat_<float>(img),Mat::zeros(img.size(), CV_32F)};

    Mat img2;

    merge(planes,2,img2);
    dft(img2, img2);
    split(img2, planes);

    Mat mag;
    magnitude(planes[0], planes[1], mag);
    mag = appplyLogTransform(mag);



    namedWindow( "mag", WINDOW_KEEPRATIO);
    namedWindow( "img", WINDOW_KEEPRATIO);
    namedWindow( "img1", WINDOW_KEEPRATIO);

    int xc =87;
    int yc = 87;
    int radius = 47;

    createTrackbar("xc", "mag", &xc,disk.cols,0 );
    createTrackbar("yc", "mag", &yc,disk.rows,0 );
    createTrackbar("radius", "mag", &radius,disk.cols,0 );

    for (int x=0; x<disk.cols;x++)
        for(int y=0;y<disk.rows;y++)
            if ((x-xc)*(x-xc)+(y-yc)*(y-yc) <= radius*radius)
                disk.at<float>(y,x)=1;
    Mat planes1[2];
    planes1[0]= planes[0].clone();
    planes1[1]= planes[1].clone();

    Mat mag1=mag.clone();
    for (;;)
    {
        disk = disk0.clone();
        disk = createWhiteDisk(img.cols,img.rows,xc,yc,radius);
        disk = ffshift(disk);

        for (int x=0; x<disk.cols;x++)
            for(int y=0;y<disk.rows;y++)
            {
                planes1[0].at<float>(y, x) = planes[0].at<float>(y, x) * disk.at<float>(y,x);
                planes1[1].at<float>(y, x) = planes[1].at<float>(y, x) * disk.at<float>(y,x);
            }

        merge(planes1,2,img2);
        idft(img2,img2,DFT_REAL_OUTPUT);
        img2 = ffshift(img2);

        for (int x=0; x<disk.cols;x++)
            for(int y=0;y<disk.rows;y++)
                mag1.at<float>(y, x) = mag.at<float>(y, x) * disk.at<float>(y,x);
        //multiply(planes[0], disk,planes[0]);
        //multiply(planes[1], disk,planes[1]);
        //merge(planes,2,img2);

        //idft(mag1,mag1,DFT_REAL_OUTPUT);
        imshow("img1", scaleImage2_uchar(img));
        imshow("mag", ffshift(scaleImage2_uchar(mag1)));
        imshow("img", ffshift(scaleImage2_uchar(img2)));
        if((char)waitKey(1)=='q') break;
    }
}
