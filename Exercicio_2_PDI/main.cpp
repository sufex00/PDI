#include <iostream>
#include <opencv2/opencv.hpp>
#include"history.h"

using namespace std;
using namespace cv;

void zerosMat(Mat *obj_img)
{
    for(int i=0 ; i < 256 ; i++)
        for(int j=0 ; j < 256; j++)
            obj_img->at<uchar>(i, j) = (uchar) 255;
}

void drawGraphics(Mat *obj_img, int r1, int s1, int r2, int s2)
{
    zerosMat(obj_img);
    line(*obj_img, Point(0, 255), Point(r1, 255-s1), 0, 4);
    line(*obj_img, Point(255, 0), Point(r2, 255-s2), 0, 4);
    line(*obj_img, Point(r1, 255-s1), Point(r2, 255-s2), 0, 4);
    circle(*obj_img, Point(r1, 255-s1), 8, 0, -1);
    circle(*obj_img, Point(r2, 255-s2), 8, 0, -1);
}

Mat transGraphics(Mat img, int r1, int s1, int r2, int s2)
{
    Mat obj_img = img.clone();
    float b;
    float a;
    int x;
    for(int i=0 ; i < obj_img.cols ; i++)
    {
        for(int j=0 ; j < obj_img.rows; j++)
        {
            x = obj_img.at<uchar>(i, j);
            if(x <= (uchar) r1)
            {
                b = 0;
                if(r1 == 0)
                {
                    a = 1;
                }
                else
                {
                    a = ((float) s1)/r1;
                }
            }
            if(x > r1 && x < r2)
            {
                b = ((float)(s2*r1-s1*r2))/(r1-r2);
                a = ((float)(s1-b))/r1;
            }
            if(x >= r2)
            {
                if(r2 == 255)
                {
                    b = 0;
                }
                else
                {
                    b = ((float)(255*s2-255*r2))/(255-r2);
                }
                a = ((float)(255-b))/255;
            }

            obj_img.at<uchar>(i, j) = (uchar)(a * x + b);
        }
    }
    return obj_img;
}

int main()
{
    namedWindow("fig", WINDOW_KEEPRATIO);
    namedWindow("new_fig", WINDOW_KEEPRATIO);
    namedWindow("hist_fig", WINDOW_KEEPRATIO);
    namedWindow("hist_new_fig", WINDOW_KEEPRATIO);
    namedWindow("control", WINDOW_KEEPRATIO);
    //cv::Mat img = imread("../ctskull.tif", CV_LOAD_IMAGE_GRAYSCALE);
    cv::Mat img = imread("/home/pedro/PDI/PDI/img/pollen.jpg", CV_LOAD_IMAGE_GRAYSCALE);
    cv::Mat hist;
    cv::Mat img_new = img.clone();
    cv::Mat graphics;

    int row, col;
    row = 256;
    col = 256;

    graphics.create(row, col, CV_8UC1);
    History history ;
    int s1 = 65, s2 = 195, r1 = 65, r2 = 195;

    cv::createTrackbar("t_r1", "control", &r1, 255, 0, 0);
    cv::createTrackbar("t_s1", "control", &s1, 255, 0, 0);
    cv::createTrackbar("t_r2", "control", &r2, 255, 0, 0);
    cv::createTrackbar("t_s2", "control", &s2, 255, 0, 0);

    for(;;)
    {
        //int isEqual = !norm(img, img_new, NORM_L1);
        hist = history.computeHistogram1C(img);
        drawGraphics(&graphics, r1, s1, r2, s2);
        img_new = transGraphics(img, r1, s1, r2, s2);
        //printf("%d", isEqual);
        cv::imshow("new_fig", img_new);
        cv::imshow("hist_new_fig", history.computeHistogram1C(img_new));
        cv::imshow("fig", img);
        cv::imshow("hist_fig", history.computeHistogram1C(img));
        cv::imshow("control", graphics);

        if((char) waitKey(1)=='q') return 0;
    }
}
