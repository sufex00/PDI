#include "history.h"

History::History()
{

}

cv::Mat History::computeHistogram1C (const cv::Mat &src)
{
    /// Establish the number of bins
    int histSize = 256;

    /// Set the ranges ( for B,G,R) )
    float range[] = { 0, 256 } ;
    const float* histRange = { range };

    bool uniform = true; bool accumulate = false;

    Mat b_hist/*, g_hist, r_hist*/;

    /// Compute the histograms:
    calcHist( &src, 1, 0, Mat(), b_hist, 1, &histSize, &histRange, uniform, accumulate );


    // Draw the histograms for B, G and R
    int hist_w = 512; int hist_h = 400;
    int bin_w = cvRound( (double) hist_w/histSize );

    Mat histImage( hist_h, hist_w, CV_8UC1, Scalar( 0 ) );

    /// Normalize the result to [ 0, histImage.rows ]
    normalize(b_hist, b_hist, 0, histImage.rows, NORM_MINMAX, -1, Mat() );

    /// Draw for each channel
    for( int i = 1; i < histSize; i++ )
    {
        line( histImage, Point( bin_w*(i-1), hist_h - cvRound(b_hist.at<float>(i-1)) ) ,
              Point( bin_w*(i), hist_h - cvRound(b_hist.at<float>(i)) ),
              Scalar( 255 ), 2, 8, 0  );
    }

    return histImage;

}


cv::Mat History::computeHistogram3C (const cv::Mat &src)
{
    cv::Mat img_output;
    vector<Mat> img_chanel;
    split(src, img_chanel);

    img_chanel[0] = this->computeHistogram1C (img_chanel[0]);
    img_chanel[1] = this->computeHistogram1C (img_chanel[1]);
    img_chanel[2] = this->computeHistogram1C (img_chanel[2]);

    cv::merge(img_chanel.data(), 3, img_output);
    return img_output;

    //imshow("b",bgr[0]);
    //imshow("g",bgr[1]);
    //imshow("r",bgr[2]);

}
