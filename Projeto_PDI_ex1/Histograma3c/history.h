#ifndef HISTORY_H
#define HISTORY_H

#include "definition.h"

class History
{
public:
    History();

    cv::Mat computeHistogram1C (const cv::Mat &src);

    cv::Mat computeHistogram3C (const cv::Mat &src);
};

#endif // HISTORY_H
