#include"definition.h"

int main()
{
    History obj_history = History();
    cv::Mat img = imread("../baboon.png", CV_LOAD_IMAGE_COLOR);
    namedWindow("fig", CV_WINDOW_AUTOSIZE);
    cv::Mat history ;
    if (!img.data)
    {
        qDebug() << "Image not loaded" << endl;
        return -1;
    }
    history = obj_history.computeHistogram3C(img);
    cv::imshow("fig",history);

    for(;;)if(waitKey(5)=='q') return 1;

    return 0;

}
