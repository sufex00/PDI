#include <iostream>
#include<stdio.h>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

void on_button(int, void*){
    printf("click\n");
}

int main(int argc, char *argv[])
{
    namedWindow( "img", WINDOW_KEEPRATIO);
    Mat img  = imread ("/home/pedro/PDI/PDI/img/lena.png", IMREAD_GRAYSCALE);
    Mat img2;
    int filtro = 1;
    int size = 0;
    createTrackbar("sel_filtro", "img", &filtro, 4, 0, 0 );
    createTrackbar("sel_size", "img", &size, 100, 0, 0 );
    //putText(img, "pedro", Point(30, 30), FONT_HERSHEY_COMPLEX,1,  Scalar(255, 255, 255) );
    for(;;)
    {
        if(filtro == 0)
        {
            img2 =img.clone();
            putText(img2, "Normal", Point(30, 30), FONT_HERSHEY_COMPLEX,1,  Scalar(255, 255, 255) );
        }
        if(filtro == 1)
        {
            blur(img, img2, Size(size+1, size+1));
            putText(img2, "Media", Point(30, 30), FONT_HERSHEY_COMPLEX,1,  Scalar(255, 255, 255) );
        }
        if(filtro == 2)
        {
            medianBlur(img, img2, 2*size+1);
            putText(img2, "Mediana", Point(30, 30), FONT_HERSHEY_COMPLEX,1,  Scalar(255, 255, 255) );
        }
        if(filtro == 3)
        {
            GaussianBlur(img, img2, Size(2*size+1, 2*size+1), 16);
            putText(img2, "Gaussiano", Point(30, 30), FONT_HERSHEY_COMPLEX,1,  Scalar(255, 255, 255) );
        }
        if(filtro == 4)
        {
            //bilateralFilter(img, img2, 2*size+1);
            bilateralFilter(img, img2, 10, size, size*4);
            putText(img2, "Bilateral", Point(30, 30), FONT_HERSHEY_COMPLEX,1,  Scalar(255, 255, 255) );
        }
        imshow("img", img2);
        if((char) waitKey(1)=='q') return 0;
    }
}
